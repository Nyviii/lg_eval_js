À refaire :
	◦	Trouver un événement pour que la personne ne puisse pas mettre de lettre dans la zone de prix
	◦	Désigner des div pour le résultat pour que ça soit joli (framework CSS)
	◦	Designer le tout pour que ce soit ergonomique 
	◦	Ajouter des labels aux inputs 
	◦	Bouton « valider » à droite et l’appeler « ajouter le menu à la carte »
	◦	Bouton « supprimer » plutôt « réinitialiser la carte »
	◦	Section à designer
    ◦   Articles séparés avec une petite croix pour en supprimer un seul
    ◦   Chercher comment supprimer une balise
    ◦   La div est l’élément enfant du parent article
	◦	Pour rendre une truc transparent : rgba 
	◦	Text-shadow pour ajouter une ombre à un texte qui donne un effet de contraste
	◦	Réinitialiser les inputs entre deux remplissages